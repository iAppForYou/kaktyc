//
//  TeacherVC.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 09.04.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth
import MultipeerConnectivity
import GameKit

var connectedPeersNames = NSMutableArray()

var Type="Тест"
var PickedTheme = String()
class TeacherSettingsVC: UITableViewController, MCSessionDelegate, MCBrowserViewControllerDelegate {
    
    let serviceType = "LCOC-Chat"
    
    var browser : MCBrowserViewController!
    var assistant : MCAdvertiserAssistant!
    var session : MCSession!
    var peerID: MCPeerID!
    
// 1 Тест/Зачет/Экзамен
    func cellTouch0(cell:UITableViewCell){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let test = UIAlertAction(title: "Тест", style: .Default, handler: { (action) -> Void in
            cell.textLabel?.text = "Тест"
        })
        let exam = UIAlertAction(title: "Экзамен", style: .Default, handler: { (action) -> Void in
            cell.textLabel?.text = "Экзамен"
        })
        let offset = UIAlertAction(title: "Зачет", style: .Default, handler: { (action) -> Void in
            cell.textLabel?.text = "Зачет"
        })
        let cancel = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
        })
        alert.addAction(test)
        alert.addAction(exam)
        alert.addAction(offset)
        alert.addAction(cancel)
        presentViewController(alert, animated: true, completion: nil)
    }
    func cellReload0(cell:UITableViewCell){
        
    }
    
// 2 Тема/Выберите тему
    func cellTouch1(cell:UITableViewCell){
        //let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TeacherThemePickVC") as! TeacherThemePickVC
        //self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    func cellReload1(cell:UITableViewCell){
        if PickedTheme != String() {
        cell.textLabel?.text = PickedTheme
        }else{
        cell.textLabel?.text = "Выберите тему"
        }
    }
    
// 3 Дополнительный вопрос/Выберите вопрос
    func cellTouch2(cell:UITableViewCell){
    showBrowser()
    }
    func cellReload2(cell:UITableViewCell){
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        connectedPeersNames.removeAllObjects()
        self.peerID = MCPeerID(displayName: "Учитель")
        self.session = MCSession(peer: peerID)
        self.session.delegate = self
        
        // create the browser viewcontroller with a unique service name
        self.browser = MCBrowserViewController(serviceType:serviceType,
            session:self.session)
        
        self.browser.delegate = self
        
        self.navigationController?.navigationBar.topItem?.title = ""
        //self.assistant = MCAdvertiserAssistant(serviceType:serviceType,
           // discoveryInfo:nil, session:self.session)
        
        // tell the assistant to start advertising our fabulous chat
        //self.assistant.start()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   // override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //if(section == 0){return 2}
        //if(section == 3) {return (1 + self.session.connectedPeers.count)}
        //return 1
    //}
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section==0 && indexPath.row==0){cellReload0(cell)}
        if(indexPath.section==1 && indexPath.row==0){cellReload1(cell)}
        if(indexPath.section==2 && indexPath.row==0){cellReload2(cell)}
    }
    
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if(indexPath.section==0 && indexPath.row==0){self.cellTouch0(cell!)}
        if(indexPath.section==1 && indexPath.row==0){self.cellTouch1(cell!)}
        if(indexPath.section==2 && indexPath.row==0){self.cellTouch2(cell!)}
    }
    
    /*
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        cell.accessoryType=UITableViewCellAccessoryType.None
        if(Type == "Зачет" && indexPath.section==0 && indexPath.row==0){cell.accessoryType=UITableViewCellAccessoryType.Checkmark}// 0-0
        else if(Type == "Экзамен" && indexPath.section==0 && indexPath.row==1){cell.accessoryType=UITableViewCellAccessoryType.Checkmark}// 0-1
        else if(indexPath.section==1 && indexPath.row==0){// 1-0
            if(PickedTheme==""){cell.textLabel?.text="Выберите тему"
            cell.accessoryType=UITableViewCellAccessoryType.DisclosureIndicator
            }else{
            cell.textLabel?.text=PickedTheme
            cell.accessoryType=UITableViewCellAccessoryType.Checkmark
            }
        }else if(indexPath.section==2 && indexPath.row==0){cell.accessoryType=UITableViewCellAccessoryType.DisclosureIndicator}// 2-0
        else if(indexPath.section==3 && indexPath.row != 0){
            //var cp = self.session.connectedPeers.obj
            cell.accessoryType=UITableViewCellAccessoryType.Checkmark
            cell.textLabel?.text="\(connectedPeersNames.objectAtIndex(indexPath.row-1))"
        }
    }
*/
    /*
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section==0 && indexPath.row==0){Type = "Зачет"}// 0-0
        else if(indexPath.section==0 && indexPath.row==1){Type = "Экзамен"}// 0-1
        else if(indexPath.section==1 && indexPath.row==0){// 1-0
let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TeacherThemePickVC") as! TeacherThemePickVC
self.navigationController?.pushViewController(secondViewController, animated: true)
        }else if(indexPath.section==3 && indexPath.row==0){// 3-0
            showBrowser()
        }else if(indexPath.section==4 && indexPath.row==0){// 4-0
            if(PickedTheme == ""){
                PKHUD.sharedHUD.contentView = PKHUDSubtitleView(subtitle: "Не выбрана тема", image: PKHUDAssets.crossImage)
                PKHUD.sharedHUD.show()
                PKHUD.sharedHUD.hide(afterDelay: 2.0)
            }else{
                sendTest()
                let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TeacherTestVC") as! TeacherTestVC
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
        tableView.reloadData()
        
    }
    */
    
    
    func sendConnected() {
        // Bundle up the text in the message field, and send it off to all
        // connected peers
        
        let msg = "Connected".dataUsingEncoding(NSUTF8StringEncoding,
            allowLossyConversion: false)
        
        var error : NSError?
        
        self.session.sendData(msg, toPeers: self.session.connectedPeers,
            withMode: MCSessionSendDataMode.Unreliable, error: &error)
        
        if error != nil {
            print("Error sending data: \(error?.localizedDescription)")
        }
        
        //self.updateChat(self.messageField.text, fromPeer: self.peerID)
        
    }
    
    func sendTest(){
        let msg = PickedTheme.dataUsingEncoding(NSUTF8StringEncoding,
            allowLossyConversion: false)
        
        var error : NSError?
        
        self.session.sendData(msg, toPeers: self.session.connectedPeers,
            withMode: MCSessionSendDataMode.Unreliable, error: &error)
        
        if error != nil {
            print("Error sending data: \(error?.localizedDescription)")
        }
        
    }
    /*

    
    func updateChat(text : String, fromPeer peerID: MCPeerID) {
        // Appends some text to the chat view
        
        // If this peer ID is the local device's peer ID, then show the name
        // as "Me"
        var name : String
        
        switch peerID {
        case self.peerID:
            name = "Me"
        default:
            name = peerID.displayName
        }
        
        // Add the name to the message and display it
        let message = "\(name): \(text)\n"
        self.chatView.text = self.chatView.text + message
        
    }*/
    
     func showBrowser() {
        // Show the browser view controller
        
        self.presentViewController(self.browser, animated: true, completion: nil)
    }
    
    func browserViewControllerDidFinish(
        browserViewController: MCBrowserViewController!)  {
            // Called when the browser view controller is dismissed (ie the Done
            // button was tapped)
            println("browserViewControllerDidFinish")
            self.dismissViewControllerAnimated(true, completion: nil)
            
            sendConnected()
    }
    
    func browserViewControllerWasCancelled(
        browserViewController: MCBrowserViewController!)  {
            // Called when the browser view controller is cancelled
            println("browserViewControllerWasCancelled")
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func session(session: MCSession!, didReceiveData data: NSData!,
        fromPeer peerID: MCPeerID!)  {
            // Called when a peer sends an NSData to us
            
            // This needs to run on the main queue
            dispatch_async(dispatch_get_main_queue()) {
                
                //var msg = NSString(data: data, encoding: NSUTF8StringEncoding) as! String
                
                var a = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! NSArray
                println(a)
                
                if(a.objectAtIndex(0) as! String == "Progress"){
                    //connectedPeersNames.indexOfObject(peerID.displayName)
                    
                    connectedPeersProgress.replaceObjectAtIndex(connectedPeersNames.indexOfObject(peerID.displayName), withObject: a.objectAtIndex(1))
                }else if(a.objectAtIndex(0) as! String == "Rating"){
                    connectedPeersRating.replaceObjectAtIndex(connectedPeersNames.indexOfObject(peerID.displayName), withObject: a.objectAtIndex(1))
                    let alert = UIAlertView(title: "\(peerID.displayName)", message: "Оценка:\(a.objectAtIndex(1))", delegate: self, cancelButtonTitle: "Хорошо")
                    alert.show()
                }
                NSNotificationCenter.defaultCenter().postNotificationName("reloadTeacherTest", object: nil)
                /*
                if(msg.lastPathComponent == "S"){
                    println("msg")
                }else{
                let alert = UIAlertView(title: "\(peerID.displayName)", message: "Оценка:\(msg)", delegate: self, cancelButtonTitle: "Хорошо")
                alert.show()
                }*/
                //self.updateChat(msg, fromPeer: peerID)
            }
    }
    
    // The following methods do nothing, but the MCSessionDelegate protocol
    // requires that we implement them.
    func session(session: MCSession!,
        didStartReceivingResourceWithName resourceName: String!,
        fromPeer peerID: MCPeerID!, withProgress progress: NSProgress!)  {
            
            // Called when a peer starts sending a file to us
    }
    
    func session(session: MCSession!,
        didFinishReceivingResourceWithName resourceName: String!,
        fromPeer peerID: MCPeerID!,
        atURL localURL: NSURL!, withError error: NSError!)  {
            // Called when a file has finished transferring from another peer
    }
    
    func session(session: MCSession!, didReceiveStream stream: NSInputStream!,
        withName streamName: String!, fromPeer peerID: MCPeerID!)  {
            // Called when a peer establishes a stream with us
    }
    
    func session(session: MCSession!, peer peerID: MCPeerID!,
        didChangeState state: MCSessionState)  {
            // Called when a connected peer changes state (for example, goes offline)
            if(state == MCSessionState.Connected){
            connectedPeersNames.addObject(peerID.displayName)
            }else if(state == MCSessionState.NotConnected){
            connectedPeersNames.removeObject(peerID.displayName)
            }
            tableView.reloadData()
    }
    //////--------------------
    
    func readjson(fileName: String) -> NSData{
        
        let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "json")
        let jsonData = NSData(contentsOfMappedFile: path!)
        
        return jsonData!
    }
    

}

