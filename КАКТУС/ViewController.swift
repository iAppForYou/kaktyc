//
//  ViewController.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 09.04.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth
import MultipeerConnectivity
import GameKit

var PupilName = String()
var PupilGroup = String()

class ViewController: UIViewController{//, MCSessionDelegate, MCBrowserViewControllerDelegate {
    /*
    let serviceType = "LCOC-Chat"
    
    var browser : MCBrowserViewController!
    var assistant : MCAdvertiserAssistant!
    var session : MCSession!
    var peerID: MCPeerID!
    
    @IBOutlet var chatView: UITextView!
    @IBOutlet var messageField: UITextField!
*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = self.view.viewWithTag(2) as! UIImageView
        imageView.image = imageView.image?.applyLightEffect()
        
        /*
        var json: AnyObject! = NSJSONSerialization.JSONObjectWithData(readjson("Молекулярная физика"), options: NSJSONReadingOptions.MutableContainers, error: nil)
        
        if let dict = json as? [String: AnyObject] { //  открыл json
            var name = dict["Name"] as String
            //println(name)
            
            let Qestions: AnyObject = dict["Qestions"]!
        for array in Qestions as [String: AnyObject] {
            var rightAnswer = array.1["rightAnswer"] as String //  открыл вопросы
                    //println(rightAnswer)
            let otherAnswers: AnyObject = array.1["otherAnswers"]!!
            for array2 in otherAnswers as [String: AnyObject] {
                var otherAnswer = array2.1 as String //  открыл остальные ответы
                println(otherAnswer)
            }
            
        }
            
        }
         */
        /*
        self.peerID = MCPeerID(displayName: UIDevice.currentDevice().name)
        self.session = MCSession(peer: peerID)
        self.session.delegate = self
        
        // create the browser viewcontroller with a unique service name
        self.browser = MCBrowserViewController(serviceType:serviceType,
            session:self.session)
        
        self.browser.delegate = self
        
        self.assistant = MCAdvertiserAssistant(serviceType:serviceType,
            discoveryInfo:nil, session:self.session)
        
        // tell the assistant to start advertising our fabulous chat
        self.assistant.start()
        */
        
        

    }
    /*
    @IBAction func sendChat(sender: UIButton) {
        // Bundle up the text in the message field, and send it off to all
        // connected peers
        
        let msg = self.messageField.text.dataUsingEncoding(NSUTF8StringEncoding,
            allowLossyConversion: false)
        
        var error : NSError?
        
        self.session.sendData(msg, toPeers: self.session.connectedPeers,
            withMode: MCSessionSendDataMode.Unreliable, error: &error)
        
        if error != nil {
            print("Error sending data: \(error?.localizedDescription)")
        }
        
        self.updateChat(self.messageField.text, fromPeer: self.peerID)
        
        self.messageField.text = ""
    }
    
    func updateChat(text : String, fromPeer peerID: MCPeerID) {
        // Appends some text to the chat view
        
        // If this peer ID is the local device's peer ID, then show the name
        // as "Me"
        var name : String
        
        switch peerID {
        case self.peerID:
            name = "Me"
        default:
            name = peerID.displayName
        }
        
        // Add the name to the message and display it
        let message = "\(name): \(text)\n"
        self.chatView.text = self.chatView.text + message
        
    }
    
    @IBAction func showBrowser(sender: UIButton) {
        // Show the browser view controller
        self.presentViewController(self.browser, animated: true, completion: nil)
    }
    
    func browserViewControllerDidFinish(
        browserViewController: MCBrowserViewController!)  {
            // Called when the browser view controller is dismissed (ie the Done
            // button was tapped)
            println("browserViewControllerDidFinish")
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(
        browserViewController: MCBrowserViewController!)  {
            // Called when the browser view controller is cancelled
            println("browserViewControllerWasCancelled")
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func session(session: MCSession!, didReceiveData data: NSData!,
        fromPeer peerID: MCPeerID!)  {
            // Called when a peer sends an NSData to us
            
            // This needs to run on the main queue
            dispatch_async(dispatch_get_main_queue()) {
                
                var msg = NSString(data: data, encoding: NSUTF8StringEncoding) as! String
                
                self.updateChat(msg, fromPeer: peerID)
            }
    }
    
    // The following methods do nothing, but the MCSessionDelegate protocol
    // requires that we implement them.
    func session(session: MCSession!,
        didStartReceivingResourceWithName resourceName: String!,
        fromPeer peerID: MCPeerID!, withProgress progress: NSProgress!)  {
            
            // Called when a peer starts sending a file to us
    }
    
    func session(session: MCSession!,
        didFinishReceivingResourceWithName resourceName: String!,
        fromPeer peerID: MCPeerID!,
        atURL localURL: NSURL!, withError error: NSError!)  {
            // Called when a file has finished transferring from another peer
    }
    
    func session(session: MCSession!, didReceiveStream stream: NSInputStream!,
        withName streamName: String!, fromPeer peerID: MCPeerID!)  {
            // Called when a peer establishes a stream with us
    }
    
    func session(session: MCSession!, peer peerID: MCPeerID!,
        didChangeState state: MCSessionState)  {
            // Called when a connected peer changes state (for example, goes offline)
            
    }
*/
//////--------------------

    func readjson(fileName: String) -> NSData{
        
        let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "json")
        let jsonData = NSData(contentsOfMappedFile: path!)
        
        return jsonData!
    }
    
    @IBAction func buttonPupilUp(sender: AnyObject) {
        // Add a text field
        
        var inputTextField1: UITextField?
        var inputTextField2: UITextField?
        let alert = UIAlertController(title: "Войдите", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder="ФИО"
            inputTextField1 = textField
            // Here you can configure the text field (eg: make it secure, add a placeholder, etc)
        }
        alert.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder="Группа"
            inputTextField2 = textField
            // Here you can configure the text field (eg: make it secure, add a placeholder, etc)
        }
        let cancel = UIAlertAction(title: "Отмена", style: .Default, handler: { (action) -> Void in
        })
        let ok = UIAlertAction(title: "Продолжить", style: .Default, handler: { (action) -> Void in
            if (inputTextField1?.text == "" || inputTextField2?.text == ""){
            }else{
            PupilName = inputTextField1!.text
            PupilGroup = inputTextField2!.text
                let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PupilBluetoothVC") as! PupilBluetoothVC
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        })
        alert.addAction(cancel)
        alert.addAction(ok)
        presentViewController(alert, animated: true, completion: nil)
        
        
        /*
        let alert = SCLAlertView()
        let txt = alert.addTextField(title:"ФИО")
        alert.addButton("Продолжить") {
            println("Text value: \(txt.text)")
            if (txt.text == ""){}else{
                PupilName=txt.text
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PupilVC") as PupilVC
            self.navigationController?.pushViewController(secondViewController, animated: true)
            }
 
        }
        alert.showEdit("Введите ФИО", subTitle:"", closeButtonTitle:"Отмена")
*/
        //let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PupilVC") as PupilVC
        //self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }

}


