//
//  PupilQuestionsVC.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 15.04.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit

class PupilQuestionsVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Question.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        
        cell.textLabel?.text="\(indexPath.row+1). \(Question.objectAtIndex(indexPath.row))"
        cell.textLabel?.numberOfLines=0
        
        if(PupilAnswers.objectAtIndex(indexPath.row) as! String == "NULL"){
            cell.accessoryType=UITableViewCellAccessoryType.None
        }else{
            cell.accessoryType=UITableViewCellAccessoryType.Checkmark
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        intQuestionNumber = indexPath.row+1
        
        self.navigationController?.popViewControllerAnimated(true)
        //cell?.backgroundColor=UIColor.redColor()
        //PickedTheme = allThemes.objectAtIndex(indexPath.row) as! String
        //self.navigationController?.popViewControllerAnimated(true)
    }
}
