//
//  TeacherTest.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 20.04.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit
var connectedPeersProgress = NSMutableArray()
var connectedPeersRating = NSMutableArray()
class TeacherTestVC: UIViewController{
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        connectedPeersProgress.removeAllObjects()
        connectedPeersRating.removeAllObjects()
        
        for(var i=0;i<connectedPeersNames.count; i+=1){
         connectedPeersProgress.addObject(0)
        connectedPeersRating.addObject(0)
        }
        
        let backItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        navigationItem.setHidesBackButton(true, animated: true)
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "Notification:",
            name: "reloadTeacherTest",
            object: nil)
    }
    
    func Notification(notification: NSNotification){
        if(notification.name == "reloadTeacherTest"){
            tableView.reloadData()
            var n=0
            for(var i=0; i<connectedPeersRating.count; i+=1){
                if(connectedPeersRating.objectAtIndex(i) as! Int != 0){
                  n+=1
                }
            }
            if(n == connectedPeersRating.count){
                println("WIN")
                let backItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
                navigationItem.setHidesBackButton(false, animated: true)
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return connectedPeersNames.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        if(connectedPeersRating.objectAtIndex(indexPath.row) as! Int == 0){
        cell.textLabel?.text=connectedPeersNames.objectAtIndex(indexPath.row) as? String
        }else{
        cell.textLabel?.textColor=UIColor(red: (0/256), green: (73/256), blue: (154/256), alpha: 1.0)
        cell.textLabel?.text="Оценка:\(connectedPeersRating.objectAtIndex(indexPath.row))  \(connectedPeersNames.objectAtIndex(indexPath.row))"
        }
        
        let progress = cell.viewWithTag(1) as! UIProgressView
        var f = connectedPeersProgress.objectAtIndex(indexPath.row) as! Float
        println("f=\(f)")
        progress.setProgress(f, animated: true)
        
        return cell
    }

}
