//
//  PupilVC.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 09.04.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit
var questions=NSMutableArray()

var count=4

var Question = NSMutableArray()
var RightAnswer = NSMutableArray()
var OtherAnswers = NSMutableArray()

var PupilAnswers = NSMutableArray()

var trueAnswers = NSMutableArray()

var intQuestionNumber = Int()
var rating = Int()

class PupilTestVC: UIViewController {
    @IBOutlet weak var LabelInitial: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelQuestionNumber: UILabel!
    
    @IBOutlet weak var buttonEnd: UIButton!
    
    var Qestions = [String: AnyObject]()//
    //var AllAnswers = NSMutableArray() // //
    //var AllRightAnswers = NSMutableArray()//
    var otherAnswers = [String: AnyObject]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
        labelQuestionNumber.text="\(intQuestionNumber)"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonEnd.hidden=true
        
        tableView.estimatedRowHeight = 100.0
         tableView.rowHeight = UITableViewAutomaticDimension

        rating = 0
        intQuestionNumber=1
        labelQuestionNumber.text="\(intQuestionNumber)"
        
        PupilAnswers.removeAllObjects()
        let backItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        navigationItem.setHidesBackButton(true, animated: true)
        
        
        var json: AnyObject! = NSJSONSerialization.JSONObjectWithData(ViewController().readjson(TestTheme), options: NSJSONReadingOptions.MutableContainers, error: nil)
        
        if let dict = json as? [String: AnyObject] { //  открыл json
            var name = dict["Name"] as! String
            labelName.text=name
            //println(name)
            
            Qestions = dict["Qestions"]! as! [String: AnyObject]
            
            for array in Qestions as [String: AnyObject] {
                var rightAnswer = array.1["rightAnswer"] as! String //  открыл вопросы
                var question = array.1["question"] as! String
                //println(rightAnswer)
                //AllRightAnswers.addObject(rightAnswer)
                //AllQestions.addObject(question)
                
                
                otherAnswers = array.1["otherAnswers"]! as! [String: AnyObject]
                //println(otherAnswers)
                for array2 in otherAnswers as [String: AnyObject] {
                    var otherAnswer = array2.1 as! String //  открыл остальные ответы
                    //println(otherAnswer)
                }
                
            }
            
        }

        buildTest()
    }
    
    
    
    func buildTest(){
        var originalQestions = [String: AnyObject]()
        let arrayQestions = NSMutableArray()
        
        var arrQestion = NSMutableArray()//
        var arrRightAnswer = NSMutableArray()//
        var arrOtherAnswers = NSMutableArray() // //
        
        for array in Qestions as [String: AnyObject] {
            var question = array.1["question"] as! String //  открыл вопросы
            var rightAnswer = array.1["rightAnswer"] as! String
            arrQestion.addObject(question)
            arrRightAnswer.addObject(rightAnswer)
            //println(rightAnswer)
            PupilAnswers.addObject("NULL")
            otherAnswers = array.1["otherAnswers"]! as! [String: AnyObject]
            let arrOtAn = NSMutableArray()
            //println(otherAnswers)
            for array2 in otherAnswers as [String: AnyObject] {
                var otherAnswer = array2.1 as! String //  открыл остальные ответы
                //println(otherAnswer)
                arrOtAn.addObject(otherAnswer)
            }
            arrOtherAnswers.addObject(arrOtAn)
        }

        Question = arrQestion//
        RightAnswer = arrRightAnswer//
        OtherAnswers = arrOtherAnswers // //
        
        //trueAnswers=OtherAnswers;
        
        for (var i=0; i<OtherAnswers.count; i+=1){
            var ar = OtherAnswers.objectAtIndex(i) as! NSMutableArray
            
            for (var x = 0; x < ar.count; x+=1) {
                var randInt = (arc4random_uniform(UInt32((ar.count - x)))) as UInt32
                randInt = randInt + UInt32(x)
                
                ar.exchangeObjectAtIndex(Int(x), withObjectAtIndex: Int(randInt))
            }
            
            ar.insertObject(RightAnswer.objectAtIndex(i), atIndex: Int(arc4random_uniform(3)))
            
            trueAnswers.addObject(ar)
        }
        //println("---------------------")
        //println(trueAnswers)
        
        //tableView.reloadData()
        
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var answ = OtherAnswers.objectAtIndex(1) as! NSMutableArray
        
        
        return answ.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        
        var ar = trueAnswers.objectAtIndex(intQuestionNumber-1) as! NSMutableArray
        
        cell.textLabel!.text="\(indexPath.row + 1). \(ar.objectAtIndex(indexPath.row))"
        cell.textLabel?.numberOfLines=0
        
        if(PupilAnswers.objectAtIndex(intQuestionNumber-1) as! String == "NULL"){
            cell.accessoryType=UITableViewCellAccessoryType.None
        }else{
            cell.accessoryType=UITableViewCellAccessoryType.None
            if(PupilAnswers.objectAtIndex(intQuestionNumber-1) as! String == ar.objectAtIndex(indexPath.row) as! String){
                cell.accessoryType=UITableViewCellAccessoryType.Checkmark
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if(intQuestionNumber < Question.count){
            intQuestionNumber+=1
        labelQuestionNumber.text="\(intQuestionNumber)"
            
            var ar = trueAnswers.objectAtIndex(intQuestionNumber-2) as! NSMutableArray
            
            PupilAnswers.replaceObjectAtIndex(intQuestionNumber-2, withObject: ar.objectAtIndex(indexPath.row))
            
            tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
            
        }else if(intQuestionNumber == Question.count){
            println("WIN")
            var ar = trueAnswers.objectAtIndex(intQuestionNumber-1) as! NSMutableArray
            
            PupilAnswers.replaceObjectAtIndex(intQuestionNumber-1, withObject: ar.objectAtIndex(indexPath.row))
            tableView.reloadData()
        }
        
        var n = 0
        for (var i = 0; i < PupilAnswers.count; i+=1){
            println(PupilAnswers.objectAtIndex(i))
            if(PupilAnswers.objectAtIndex(i) as! String == "NULL"){
                
            }else{
                n+=1
            }
        }
        //println("n=\(n)  Question.count=\(Question.count)")
        if(n==PupilAnswers.count){
            println("WIN")
            buttonEnd.hidden=false
            
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("sendProgress", object: nil)
    }

    var height  = CGFloat()
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(section == 0) {
            var view = UIView()
            view.backgroundColor=self.view.backgroundColor
            
            var label = UILabel()
            label.numberOfLines=0
            label.text=Question.objectAtIndex(intQuestionNumber-1) as? String
            label.font=UIFont.systemFontOfSize(18)
            label.lineBreakMode = .ByWordWrapping
            label.textAlignment=NSTextAlignment.Center
            
            height = heightForView(label.text!, font: label.font, width: self.view.frame.width)
            
            label.frame=CGRectMake(0, 25, self.view.frame.width, height)
            
            
            view.addSubview(label)
            
            return view
        }
        return nil
        
    }
    
    @IBAction func buttonEndUp(sender: AnyObject) {
        /*
        println(RightAnswer)
        println("---------------PupilAnswers")
        println(PupilAnswers)
        */
        
        var t = 0
        
        for (var i=0; i<PupilAnswers.count; i+=1){
            if (PupilAnswers.objectAtIndex(i) as! String == RightAnswer.objectAtIndex(i) as! String){
                t+=1
            }else{
                
            }
        }
        
        var k = Float()
        k = Float(t) / Float(PupilAnswers.count)
        
        //println("t=\(t)")
        //println("k=\(k)")
        
        if(k >= 0 && k <= 0.2){
            rating = 1
        }else if(k > 0.2 && k <= 0.4){
            rating = 2
        }else if(k > 0.4 && k <= 0.6){
            rating = 3
        }else if(k > 0.6 && k <= 0.8){
            rating = 4
        }else if(k > 0.8){
            rating = 5
        }
        
        let alert = UIAlertController(title: "Ваша оценка", message: "\(rating)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancel = UIAlertAction(title: "Хорошо", style: .Default, handler: { (action) -> Void in
            self.navigationController?.popToRootViewControllerAnimated(true)
        })
        alert.addAction(cancel)
        presentViewController(alert, animated: true, completion: nil)
        
        NSNotificationCenter.defaultCenter().postNotificationName("sendData", object: nil)
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return heightForView(Question.objectAtIndex(intQuestionNumber-1) as! String, font: UIFont.systemFontOfSize(18), width: self.view.frame.width) + 50
    }
    
    
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    
}
