//
//  TeacherThemePick.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 12.04.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit

let allThemes=NSMutableArray()
let allThemesInfo=NSMutableArray()

class TeacherThemePickVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var json: AnyObject! = NSJSONSerialization.JSONObjectWithData(ViewController().readjson("Список тем"), options: NSJSONReadingOptions.MutableContainers, error: nil)
        
        allThemes.removeAllObjects()
        getThemes()
        /*
        if let dict = json as? [String: AnyObject] { //  открыл json
            for array in dict as [String: AnyObject] {
                    var theme = array.1 as! String //  открыл остальные ответы
                allThemes.addObject(theme)
                    //println(theme)
                }
                
            }
*/
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func getThemes(){
        var list: NSMutableArray?
        if let path = NSBundle.mainBundle().pathForResource("Themes", ofType: "plist") {
            list = NSMutableArray(contentsOfFile: path)
            //println(list)
        }
        
        for var i = 0; i<list?.count; i+=1 {
            var dict = list?.objectAtIndex(i) as! NSDictionary
            var Name = dict.objectForKey("Name") as! String
            var Qestions = dict.objectForKey("Qestions") as! NSMutableArray
            allThemes.addObject(Name)
            allThemesInfo.addObject(Qestions)
            println(Name)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allThemes.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        
        cell.textLabel?.text=allThemes.objectAtIndex(indexPath.row) as? String
        
        //cell.backgroundColor=UIColor.lightGrayColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
      let cell = tableView.cellForRowAtIndexPath(indexPath)
        //cell?.backgroundColor=UIColor.redColor()
        PickedTheme = allThemes.objectAtIndex(indexPath.row) as! String
        //self.navigationController?.popViewControllerAnimated(true)
    }

    
}


