//
//  TeacherThemeCreate.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 19.06.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit
class TeacherThemeCreate: UIViewController, UITableViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    var ThemeObjects = NSMutableArray()
    
    override func viewDidLoad() {
        self.title = PickedTheme
        
        getThemeQuestions()
    }
    
    func getThemeQuestions(){
       
        var list = allThemesInfo.objectAtIndex(0) as! NSMutableArray
        
        for var i = 0; i<list.count; i+=1 {
            var dict = list.objectAtIndex(i) as! NSDictionary
            ThemeObjects.addObject(ThemeObject().initWithDictionary(dict))
            
        }
        tableView.reloadData()

    }
    
    func numberOfSectionsInTableView(tableView: UITableView?) -> Int {
        
    return ThemeObjects.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var themeObject = ThemeObjects.objectAtIndex(section) as! ThemeObject
        return themeObject.otherAnswers.count+1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        var themeObject = ThemeObjects.objectAtIndex(indexPath.row) as! ThemeObject
        
        if(indexPath.row==0){
            cell.textLabel?.text = themeObject.rightAnswer
            cell.backgroundColor = UIColor.greenColor()
        }else{
            cell.textLabel?.text = themeObject.otherAnswers.objectAtIndex(indexPath.row - 1) as? String
            cell.backgroundColor = UIColor.blueColor()
        }
        
        return cell
    }
    
    var height  = CGFloat()
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
            var view = UIView()
            view.backgroundColor=self.view.backgroundColor
            
            var themeObject = ThemeObjects.objectAtIndex(section) as! ThemeObject
            
            var label = UILabel()
            label.numberOfLines=0
            label.text = themeObject.question
            label.font=UIFont.systemFontOfSize(18)
            label.lineBreakMode = .ByWordWrapping
            label.textAlignment=NSTextAlignment.Center
            
            height = heightForView(label.text!, font: label.font, width: self.view.frame.width)
            
            label.frame=CGRectMake(0, 25, self.view.frame.width, height)
        
            view.addSubview(label)
            
            return view
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var themeObject = ThemeObjects.objectAtIndex(section) as! ThemeObject
        return heightForView(themeObject.question as String, font: UIFont.systemFontOfSize(18), width: self.view.frame.width) + 50
    }
    
    
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }

    
    @IBAction func buttonCompleateUp(sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TeacherThemePickVC") as! TeacherThemePickVC
        self.navigationController?.popToViewController(secondViewController, animated: true)
    }
}
