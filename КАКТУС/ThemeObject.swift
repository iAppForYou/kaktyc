//
//  ThemeObject.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 19.06.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import Foundation

class ThemeObject: NSObject {
    var question = String()
    var rightAnswer = String()
    var otherAnswers = NSMutableArray()
    
    func initWithDictionary(dict:NSDictionary)-> NSObject{
        question = dict.objectForKey("question") as! String
        rightAnswer = dict.objectForKey("rightAnswer") as! String
        otherAnswers = dict.objectForKey("otherAnswers") as! NSMutableArray
        return self
    }
    
}
