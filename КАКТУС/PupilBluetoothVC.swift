//
//  PupilBluetoothVC.swift
//  КАКТУС
//
//  Created by Илья Руденцов on 13.04.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth
import MultipeerConnectivity
import GameKit

var TestTheme = String()

class PupilBluetoothVC: UIViewController, MCSessionDelegate, MCBrowserViewControllerDelegate{
    
    let serviceType = "LCOC-Chat"
    
    var browser : MCBrowserViewController!
    var assistant : MCAdvertiserAssistant!
    var session : MCSession!
    var peerID: MCPeerID!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //StartBluetoothFind()
        
        self.peerID = MCPeerID(displayName: "\(PupilName as String)-\(PupilGroup as String)")
        self.session = MCSession(peer: peerID)
        self.session.delegate = self
        
        // create the browser viewcontroller with a unique service name
        // self.browser = MCBrowserViewController(serviceType:serviceType,
         //   session:self.session)
        
        //self.browser.delegate = self
        
        self.assistant = MCAdvertiserAssistant(serviceType:serviceType,
            discoveryInfo:nil, session:self.session)
        
        // tell the assistant to start advertising our fabulous chat
        self.assistant.start()
        
        PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Ожидайте соединения с учителем...")
         PKHUD.sharedHUD.userInteractionOnUnderlyingViewsEnabled=true
        PKHUD.sharedHUD.show()
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "Notification:",
            name: "sendData",
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "Notification:",
            name: "sendProgress",
            object: nil)
    }
    
    func Notification(notification: NSNotification){
        if(notification.name == "sendData"){
            sendData()
        }else if(notification.name == "sendProgress"){
            sendProgress()
        }
    }
    
    @objc func StartBluetoothFind(){
        self.peerID = MCPeerID(displayName: "fufis")
        self.session = MCSession(peer: peerID)
        self.session.delegate = self
        
        // create the browser viewcontroller with a unique service name
        self.browser = MCBrowserViewController(serviceType:serviceType,
            session:self.session)
        
        self.browser.delegate = self
        
        self.assistant = MCAdvertiserAssistant(serviceType:serviceType,
            discoveryInfo:nil, session:self.session)
        
        // tell the assistant to start advertising our fabulous chat
        self.assistant.start()
        
    }
    
    
    func sendData() {
println("sendData")
        
    var a = NSArray(array: ["Rating",rating])
    var d : NSData = NSKeyedArchiver.archivedDataWithRootObject(a)
        
    let msg = "\(rating)".dataUsingEncoding(NSUTF8StringEncoding,
    allowLossyConversion: false)
    
    var error : NSError?
    
        var data = NSData()
        
    self.session.sendData(d, toPeers: self.session.connectedPeers,
    withMode: MCSessionSendDataMode.Unreliable, error: &error)
    
    if error != nil {
    print("Error sending data: \(error?.localizedDescription)")
    }
    
println("sendData")
    
    }
    
    func sendProgress() {
        
        var n=0
        for (var i=0; i<PupilAnswers.count;i+=1){
            if(PupilAnswers.objectAtIndex(i) as! String=="NULL"){
                
            }else{
                n+=1
            }
        }
        
        
        var p = Float(Float(n) / Float(Question.count))
        
        var a = NSArray(array: ["Progress",p])
        
        
        var d : NSData = NSKeyedArchiver.archivedDataWithRootObject(a)
        
        let msg = "\(PupilAnswers.count / Question.count)".dataUsingEncoding(NSUTF8StringEncoding,
            allowLossyConversion: false)
        
        var error : NSError?
        
        var data = NSData()
        
        self.session.sendData(d, toPeers: self.session.connectedPeers,
            withMode: MCSessionSendDataMode.Unreliable, error: &error)
        
        if error != nil {
            print("Error sending data: \(error?.localizedDescription)")
        }

    }
    
    /*
    func updateChat(text : String, fromPeer peerID: MCPeerID) {
    // Appends some text to the chat view
    
    // If this peer ID is the local device's peer ID, then show the name
    // as "Me"
    var name : String
    
    switch peerID {
    case self.peerID:
    name = "Me"
    default:
    name = peerID.displayName
    }
    
    // Add the name to the message and display it
    let message = "\(name): \(text)\n"
    self.chatView.text = self.chatView.text + message
    
    }*/
    
    func showBrowser() {
        // Show the browser view controller
        self.presentViewController(self.browser, animated: true, completion: nil)
    }
    
    func browserViewControllerDidFinish(
        browserViewController: MCBrowserViewController!)  {
            // Called when the browser view controller is dismissed (ie the Done
            // button was tapped)
            println("browserViewControllerDidFinish")
            self.dismissViewControllerAnimated(true, completion: nil)
            
            
    }
    
    func browserViewControllerWasCancelled(
        browserViewController: MCBrowserViewController!)  {
            // Called when the browser view controller is cancelled
            println("browserViewControllerWasCancelled")
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func session(session: MCSession!, didReceiveData data: NSData!,
        fromPeer peerID: MCPeerID!)  {
            // Called when a peer sends an NSData to us
            
            // This needs to run on the main queue
            dispatch_async(dispatch_get_main_queue()) {
                
                var msg = NSString(data: data, encoding: NSUTF8StringEncoding) as! String
                if(msg == "Connected"){
                   PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Учитель обнаружил ваше устройство, ожидайте начала теста...")
                }else{
                   PKHUD.sharedHUD.hide(animated: true)
                    
                    PKHUD.sharedHUD.contentView = PKHUDTitleView(title: "Успешно", image: PKHUDAssets.checkmarkImage)
                    PKHUD.sharedHUD.show()
                    PKHUD.sharedHUD.hide(afterDelay: 2.0)
                    TestTheme = msg
                    
                    let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PupilTestVC") as! PupilTestVC
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
                //let alert = UIAlertView(title: msg, message: nil, delegate: self, cancelButtonTitle: "ок")
                //alert.show()
                //PKHUD.sharedHUD.hide(animated: true)
                //self.updateChat(msg, fromPeer: peerID)
            }
    }
    
    // The following methods do nothing, but the MCSessionDelegate protocol
    // requires that we implement them.
    func session(session: MCSession!,
        didStartReceivingResourceWithName resourceName: String!,
        fromPeer peerID: MCPeerID!, withProgress progress: NSProgress!)  {
            
            // Called when a peer starts sending a file to us
    }
    
    func session(session: MCSession!,
        didFinishReceivingResourceWithName resourceName: String!,
        fromPeer peerID: MCPeerID!,
        atURL localURL: NSURL!, withError error: NSError!)  {
            // Called when a file has finished transferring from another peer
    }
    
    func session(session: MCSession!, didReceiveStream stream: NSInputStream!,
        withName streamName: String!, fromPeer peerID: MCPeerID!)  {
            // Called when a peer establishes a stream with us
    }
    
    func session(session: MCSession!, peer peerID: MCPeerID!,
        didChangeState state: MCSessionState)  {
            // Called when a connected peer changes state (for example, goes offline)
            
    }
    //////--------------------

    override func viewWillDisappear(animated: Bool) {
        PKHUD.sharedHUD.hide(animated: true)
    }
    
}